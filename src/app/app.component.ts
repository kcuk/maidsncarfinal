import { Component, Input } from '@angular/core';
import {MessageService} from 'primeng/api';
import { TouchSequence } from 'selenium-webdriver';
import { NONE_TYPE, ThrowStmt } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private msg:MessageService){}
  title = 'maidscar';

  
    obj1='4%';
    obj2='45%';
    obj3='86%';
    obj4='null';
    obj5='null';
    obj6='null';
  
    disp1:string='block';
    disp2:string='block';
    disp3:string='block';
    disp4:string='none';
    disp5:string='none';
    disp6:string='none';
  
  
    count:number = 0;
  
  forwardBtnClick:boolean =false;

  b1Click(){
    if(this.forwardBtnClick == true){
      console.log(this.count,"if part executing");

      if(this.count == 0){
        this.disp1='block';
        this.disp2='block';
        this.disp3='none';
        this.disp4='none';
        this.disp5='none';
        this.disp6='block';

        this.obj6='4%';
        this.obj1='45%';
        this.obj2='86%';

        this.count=5;
      }
     else if(this.count == 1){
        this.disp1='block';
        this.disp2='block';
        this.disp3='block';
        this.disp4='none';
        this.disp5='none';
        this.disp6='none';

        this.obj1='4%';
        this.obj2='45%';
        this.obj3='86%';

        this.count=this.count - 1;
      }
      else if(this.count == 2 ){
        this.disp1='none';
        this.disp2='block';
        this.disp3='block';
        this.disp4='block';
        this.disp5='none';
        this.disp6='none';

        this.obj2='4%';
        this.obj3='45%';
        this.obj4='86%';

        this.count=this.count - 1;
      }
      else if(this.count == 3){
        this.disp1='none';
        this.disp2='none';
        this.disp3='block';
        this.disp4='block';
        this.disp5='block';
        this.disp6='none';

        this.obj3='4%';
        this.obj4='45%';
        this.obj5='86%';

        this.count=this.count - 1;
      }
      else if(this.count == 4){
        this.disp1='none';
        this.disp2='none';
        this.disp3='none';
        this.disp4='block';
        this.disp5='block';
        this.disp6='block';

        this.obj4='4%';
        this.obj5='45%';
        this.obj6='86%';

        this.count=this.count - 1;
      }
      else if(this.count == 5){
        this.disp1='block';
        this.disp2='none';
        this.disp3='none';
        this.disp4='none';
        this.disp5='block';
        this.disp6='block';

        this.obj5='4%';
        this.obj6='45%';
        this.obj1='86%';

        this.count=this.count - 1;
      }
      
      else if(this.count ==6){
        this.disp1='block';
        this.disp2='block';
        this.disp3='none';
        this.disp4='none';
        this.disp5='none';
        this.disp6='block';

        this.obj6='4%';
        this.obj1='45%';
        this.obj2='86%';

        this.count=this.count - 1;
      }
      
    }
    else{
      this.count=this.count + 1;
      console.log(this.count,"b1Clicked else part executing------------");
    
      if(this.count > 6)
      {
        this.count =this.count % 6;
      }
      else if (this.count == 1){
        this.disp6="block";
        this.disp1="block";
        this.disp2="block";
        this.disp3="none";
  
        this.obj6="4%";
        this.obj1="45%";
        this.obj2="86%";
  
      }
        else if(this.count == 2){
          this.obj5='4%';
        this.obj6='45%';
        this.obj1 = '86%';
        
        this.disp5='block';
        this.disp6='block';
        this.disp1='block';
    
        this.disp4='none';
        this.disp3='none';
        this.disp2='none';
      
        // document.getElementById("view2").scrollIntoView({block: 'start', behavior: 'smooth'});
        // window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    
        }
      
        else if(this.count == 3){
          this.obj4='4%';
          this.obj5='45%';
          this.obj6='86%';
      
          this.disp1='none';
          this.disp2='none';
          this.disp3='none';
      
          this.disp4='block';
          this.disp5='block';
          this.disp6='block';
        
        }
      
        else if(this.count == 4){
          this.obj3='4%';
          this.obj4='45%';
          this.obj5='85%';
      
          this.disp1='none';
          this.disp2='none';
          this.disp6='none';
          this.disp3='block';
          this.disp4='block';
          this.disp5='block';
      
          // this.count=this.count-1;
    
          // this.isValid2=true;
    
          // document.getElementById("view4").scrollIntoView({block: 'start', behavior: 'smooth'});
    
        }
      
        else if(this.count == 5){
          console.log("this.count == 4");
          this.obj2='4%';
          this.obj3='45%';
          this.obj4='85%';
      
          this.disp5='none';
          this.disp6='none';
          this.disp1='none';
          this.disp2='block';
          this.disp3='block';
          this.disp4='block';
      
          // this.count=this.count-1;
    
          // this.isValid2=true;
    
          // document.getElementById("view5").scrollIntoView({block: 'start', behavior: 'smooth'});
    
        }

        else if(this.count == 6){
          this.disp1='block';
          this.disp2='block';
          this.disp3='block';
          this.disp4='none';
          this.disp5='none';
          this.disp6='none';

          this.obj1='4%';
          this.obj2='45%';
          this.obj3='86%';
        }
      
    
    }
    }
    
  
  
  
  b2Click(){
    console.log("=================== b2Clicked ------------");
    this.count = this.count+1;
    console.log(this.count,"@@@@@@@@@@@@@@@@@@@@@@   this.count   @@@@@@@@@@@@@@@@@@@@@");
    this.forwardBtnClick = true;
  
if(this.count > 6){
  this.count=this.count%6;

  if(this.count == 1){
    this.disp1='none';
    this.disp5='none';
    this.disp6='none';
    this.disp4='block';
    this.disp2='block';
    this.disp3='block';

    this.obj2='4%';
    this.obj3='45%';
    this.obj4='86%';
    document.getElementById('view2').className='animate';
    document.getElementById('view3').className='animate-slow';
    document.getElementById('view4').className='animate-slows';
  }
  else if(this.count == 2){
    console.log("----------count 2 clicks---------------");
    this.disp1='none';
    this.disp2='none';
    this.disp6='none'; 

    this.disp5='block';

    this.obj3='4%';
    this.obj4='45%';
    this.obj5='86%';
    document.getElementById('view3').className='animate';
    document.getElementById('view4').className='animate-slow';
    document.getElementById('view5').className='animate-slows';
  }
  else if (this.count == 3){

    this.disp1='none';
    this.disp2='none';
    this.disp3='none';
    
    this.disp6='block';
    
    this.obj4='4%';
    this.obj5='45%';
    this.obj6='86%';
    document.getElementById('view4').className='animate';
      document.getElementById('view5').className='animate-slow';
    document.getElementById('view6').className='animate-slows';
  }

  else if(this.count == 4){

    this.disp1='block';
    this.disp2='none';
    this.disp3='none';
    this.disp4='none';

    this.obj5='4%';
    this.obj6='45%';
    this.obj1='86%';
    document.getElementById('view5').className='animate';
      document.getElementById('view6').className='animate-slow';
    document.getElementById('view1').className='animate-slows';
  }
  else if(this.count == 5){

    this.disp3='none';
    this.disp4='none';
    this.disp5='none';
    this.disp1='block';
    this.disp2='block';
    this.disp6='block';

    this.obj6='4%';
    this.obj1='45%';
    this.obj2='86%';
    document.getElementById('view6').className='animate';
    document.getElementById('view1').className='animate-slow';
    document.getElementById('view2').className='animate-slows';
  }
  else if(this.count == 6){
    this.disp4='none';
    this.disp5='none';
    this.disp6='none';

    this.disp1='block';
    this.disp2='block';
    this.disp3='block';

    this.obj1='4%';
    this.obj2='45%'
    this.obj3='86%';
    document.getElementById('view1').className='animate';
    document.getElementById('view2').className='animate-slow';
    document.getElementById('view3').className='animate-slows';
  }

}
    else if(this.count == 1){
      this.disp1='none';
      this.disp5='none';
      this.disp6='none';
      this.disp4='block';
  
      this.obj2='4%';
      this.obj3='45%';
      this.obj4='86%';
      document.getElementById('view2').className='animate';
      document.getElementById('view3').className='animate-slow';
      document.getElementById('view4').className='animate-slows';
    }
    else if(this.count == 2){
      console.log("----------count 2 clicks---------------");
      this.disp1='none';
      this.disp2='none';
      this.disp6='none'; 
  
      this.disp5='block';
  
      this.obj3='4%';
      this.obj4='45%';
      this.obj5='86%';
  
      document.getElementById('view3').className='animate';
      document.getElementById('view4').className='animate-slow';
      document.getElementById('view5').className='animate-slows';
    }
    else if (this.count == 3){
  
      this.disp1='none';
      this.disp2='none';
      this.disp3='none';
      
      this.disp6='block';
      
      this.obj4='4%';
      this.obj5='45%';
      this.obj6='86%';
      document.getElementById('view4').className='animate';
      document.getElementById('view5').className='animate-slow';
      document.getElementById('view6').className='animate-slows';
    }
  
    else if(this.count == 4){
  
      this.disp1='block';
      this.disp2='none';
      this.disp3='none';
      this.disp4='none';
  
      this.obj5='4%';
      this.obj6='45%';
      this.obj1='86%';
      document.getElementById('view5').className='animate';
      document.getElementById('view6').className='animate-slow';
      document.getElementById('view1').className='animate-slows';
    }
    else if(this.count == 5){
  
      this.disp3='none';
      this.disp4='none';
      this.disp5='none';
      this.disp1='block';
      this.disp2='block';
      this.disp6='block';

      this.obj6='4%';
      this.obj1='45%';
      this.obj2='86%';
      document.getElementById('view6').className='animate';
      document.getElementById('view1').className='animate-slow';
      document.getElementById('view2').className='animate-slows';
    }
    else if(this.count == 6){
      this.disp4='none';
      this.disp5='none';
      this.disp6='none'

      this.disp1='block';
      this.disp2='block';
      this.disp3='block';

      this.obj1='4%';
      this.obj2='45%'
      this.obj3='86%';
      document.getElementById('view1').className='animate';
      document.getElementById('view2').className='animate-slow';
      document.getElementById('view3').className='animate-slows';
    }
  }

 // display div of testimonials
 t1:string='block';
 t2:string='none';
 t3:string='none';

 testCount:number = 0;
 testForwardButtonClick:boolean =false;

 testBackwardClick(){
  console.log(this.testCount," backward button clicked ");

  if(this.testForwardButtonClick == true ){
    if(this.testCount > 3){
      this.testCount = this.testCount % 3;
      console.log( this.testCount ," == count inside the if if (this.testCount > 3) == ");

      if(this.testCount == 0){
        this.t1='none';
        this.t2='none';
        this.t3='block';

        document.getElementById('idt3').className = 'ani-active';
      }
      else if(this.testCount == 1 ){
        this.t1='block';
        this.t2='none';
        this.t3='none';

        document.getElementById('idt1').className ='ani-active';
      }
      else if(this.testCount == 2){
        this.t1='none';
        this.t2='block';
        this.t3='none';

        document.getElementById('idt2').className = 'ani-active';
      }
    }
    else if(this.testCount == 0){
      this.t1='none';
      this.t2='none';
      this.t3='block';

      document.getElementById('idt3').className ='ani-active';
      this.testCount=2;
    }
    else if(this.testCount == 1){
      this.t1='block';
      this.t2='none';   
      this.t3='none';
  
      document.getElementById('idt1').className = 'ani-active';
      this.testCount= this.testCount - 1;
     }
     else if(this.testCount == 2){
      this.t1='none';
      this.t2='block';   
      this.t3='none';
  
      document.getElementById('idt2').className = 'ani-active';
      this.testCount= this.testCount - 1;

     }
     else if(this.testCount == 3){
      this.t1='none';
      this.t2='none';   
      this.t3='block';
  
      document.getElementById('idt3').className = 'ani-active';
      this.testCount= this.testCount - 1;

     }  
  }
    
  

  else{
    
    this.testCount=this.testCount + 1;
    console.log(this.testCount,"else part");

    if(this.testCount > 3){
      this.testCount = this.testCount % 3;
      console.log(this.testCount,"inside else > then if");

      if(this.testCount == 1){
        this.t1='none';
        this.t2='none';
        this.t3='block';
  
        document.getElementById('idt3').className='ani-active';
      }

      else if(this.testCount == 3){
        this.t1='block';
        this.t2='none';
        this.t3='none';

        document.getElementById('idt1').className='ani-active';
      }

      else if(this.testCount == 2 ){
        this.t1='none';
        this.t2='block';
        this.t3='none';
  
        document.getElementById('idt2').className='ani-active';
      }
    
    
    }
    else if(this.testCount == 1){
      this.t1='none';
      this.t2='none';
      this.t3='block';

      document.getElementById('idt3').className='ani-active';
    }
    else if(this.testCount == 2){
      this.t1='none';
      this.t2='block';
      this.t3='none';

      document.getElementById('idt2').className='ani-active';
    }
    else if(this.testCount == 3){
      this.t1='block';
      this.t2='none';
      this.t3='none';
      document.getElementById('idt1').className='ani-active';
    }
  }
 }

 testForwardClick(){
  this.testForwardButtonClick = true;
  this.testCount=this.testCount + 1;

  console.log(this.testCount,"forward button clicked");

  //  this.msg.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
  if(this.testCount > 3){
    this.testCount = this.testCount % 3;

    if(this.testCount == 0){
      //  this.msg.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
      this.t1='none';
      this.t2='block';
      this.t3='none';
 
    document.getElementById('idt2').className ='ani';
     }
   else if(this.testCount == 1 ){
               this.t1='none';
               this.t2='block';
               this.t3='none';
              document.getElementById('idt2').className ='ani';
              this.testCount  = +1;
              console.log(this.testCount , " binary asignment ");
             }
    else if(this.testCount == 2 ){
              this.t1='none';
              this.t2='none';
              this.t3='block';
             document.getElementById('idt3').className ='ani';
             this.testCount  = +1;
             console.log(this.testCount , " binary asignment ");
            }     
  }

  else{
    
    console.log(this.testCount,"================= forwardCount ===============");
    if(this.testCount == 1){
      this.t1='none';
      this.t2='block';
      this.t3='none';
 
      document.getElementById('idt2').className ='ani';
    }
    else if(this.testCount == 2){
      this.t1='none';
      this.t2='none';
      this.t3='block';
 
      document.getElementById('idt3').className ='ani';
    }
    else if(this.testCount == 3){
      this.t1='block';
      this.t2='none';
      this.t3='none';
 
      document.getElementById('idt1').className ='ani';
    }
  }
  
   
 }

 clickFunc(event) {
  console.log(event);
  console.log(event.tab.textLabel, "=====================");
  if (event.tab.textLabel == "HOME") {
        document.getElementById("home").scrollIntoView({block: 'start', behavior: 'smooth'});
  }
  else if (event.tab.textLabel == "TESTIMONIALS") {
    console.log(event.tab.textLabel,"test");
    // document.getElementById("test").scrollIntoView({behavior: 'smooth',block: 'start'});
    // document.getElementById("test").scrollIntoView(true);
    window.scrollTo(0,3300);
  }
  else if (event.tab.textLabel == "CONTACT US") {
    console.log(event.tab.textLabel,"contact");
    // document.getElementById("contact").scrollIntoView({block: 'start', behavior: 'instant'});
    window.scrollTo(0,document.body.scrollHeight);
  }
  else if (event.tab.textLabel == "ABOUT US") {
    document.getElementById("about").scrollIntoView({block: 'start', behavior: 'smooth'});
  }
  else if (event.tab.textLabel == "SERVICES") {
    document.getElementById("services").scrollIntoView({block: 'start', behavior: 'smooth'});
  }

}



// ========================
mobobj1='-20%';
mobobj2='30%';
mobobj3='80%';
mobobj4='';
mobobj5='';
mobobj6='';

mobdisp1:string='block';
mobdisp2:string='block';
mobdisp3:string='block';
mobdisp4:string='none';
mobdisp5:string='none';
mobdisp6:string='none';

mobcount:number=0;

// mobisValid1:boolean=true;
// mobisValid2:boolean=true;

mobb2Clicked:boolean =false;


mobb1Click(){
  console.log("mob1Clicked ------------");
  if(this.mobb2Clicked == true){
    console.log("@################### this.mobb2Clicked inside ============="); 
    if(this.mobcount > 6){
      this.mobcount =this.mobcount % 6;
    }
    else if(this.mobcount == 1){
      this.mobdisp1='block';
      this.mobdisp2='block';
      this.mobdisp3='block';
      this.mobdisp4='none';
      this.mobdisp5='none';
      this.mobdisp6='none';

      this.mobobj1='-20%';
      this.mobobj2='30%';
      this.mobobj3='80%';

      this.mobcount=6;
    }
    else if(this.mobcount == 2){
      this.mobdisp1='none';
      this.mobdisp2='block';
      this.mobdisp3='block';
      this.mobdisp4='block';
      this.mobdisp5='none';
      this.mobdisp6='none';

      this.mobobj2='-20%';
      this.mobobj3='30%';
      this.mobobj4='80%';

      this.mobcount =this.mobcount - 1;

    }
    else if(this.mobcount == 3){
      this.mobdisp1='none';
      this.mobdisp2='none';
      this.mobdisp3='block';
      this.mobdisp4='block';
      this.mobdisp5='block';
      this.mobdisp6='none';

      this.mobobj3='-20%';
      this.mobobj4='30%';
      this.mobobj5='80%';

      this.mobcount =this.mobcount - 1;
    }
    else if(this.mobcount == 4){
      this.mobdisp1='none';
      this.mobdisp2='none';
      this.mobdisp3='none';
      this.mobdisp4='block';
      this.mobdisp5='block';
      this.mobdisp6='block';

      this.mobobj4='-20%';
      this.mobobj5='30%';
      this.mobobj6='80%';

      this.mobcount =this.mobcount - 1;

    }
    else if(this.mobcount == 5){
      this.mobdisp1='block';
      this.mobdisp2='none';
      this.mobdisp3='none';
      this.mobdisp4='none';
      this.mobdisp5='block';
      this.mobdisp6='block';

      this.mobobj5='-20%';
      this.mobobj6='30%';
      this.mobobj1='80%';

      this.mobcount =this.mobcount - 1;

    }
    else if(this.mobcount == 6){
      this.mobdisp1='block';
      this.mobdisp2='block';
      this.mobdisp3='none';
      this.mobdisp4='none';
      this.mobdisp5='none';
      this.mobdisp6='block';

      this.mobobj6='-20%';
      this.mobobj1='30%';
      this.mobobj2='80%';

      this.mobcount =this.mobcount - 1;
    }
  }
  else{
this.mobcount =this.mobcount+1;

if(this.mobcount > 6){
  this.mobcount = this.mobcount % 6;
}
else if(this.mobcount == 1){
this.mobdisp1='block';
this.mobdisp2='block';
this.mobdisp3='none';
this.mobdisp4='none';
this.mobdisp5='none';
this.mobdisp6='block';

this.mobobj6='-20%';
this.mobobj1='30%';
this.mobobj2='80%';
}
else if(this.mobcount == 2 ){
  this.mobdisp1='block';
  this.mobdisp2='none';
  this.mobdisp3='none';
  this.mobdisp4='none';
  this.mobdisp5='block';
  this.mobdisp6='block';
  
  this.mobobj5='-20%';
  this.mobobj6='30%';
  this.mobobj1='80%';
}
else if(this.mobcount == 3){
  this.mobdisp1='none';
  this.mobdisp2='none';
  this.mobdisp3='none';
  this.mobdisp4='block';
  this.mobdisp5='block';
  this.mobdisp6='block';
  
  this.mobobj4='-20%';
  this.mobobj5='30%';
  this.mobobj6='80%';
}
else if (this.mobcount == 4){
  this.mobdisp1='none';
  this.mobdisp2='none';
  this.mobdisp3='block';
  this.mobdisp4='block';
  this.mobdisp5='block';
  this.mobdisp6='none';
  
  this.mobobj3='-20%';
  this.mobobj4='30%';
  this.mobobj5='80%';
}
else if(this.mobcount == 5){
  this.mobdisp1='none';
  this.mobdisp2='block';
  this.mobdisp3='block';
  this.mobdisp4='block';
  this.mobdisp5='none';
  this.mobdisp6='none';
  
  this.mobobj2='-20%';
  this.mobobj3='30%';
  this.mobobj4='80%';
}
else if(this.mobcount == 6){
  this.mobdisp1='block';
  this.mobdisp2='block';
  this.mobdisp3='block';
  this.mobdisp4='none';
  this.mobdisp5='none';
  this.mobdisp6='none';
  
  this.mobobj1='-20%';
  this.mobobj2='30%';
  this.mobobj3='80%';
}
  }


}

mobb2Click(){
  console.log("=================== b2Clicked ------------");
  this.mobb2Clicked=true;
    this.mobcount = this.mobcount + 1;
    console.log(this.mobcount,"==== this.mobcount value inside the mobb2Click()  ====");

    if(this.mobcount>6){
      this.mobcount=this.mobcount%6;
      console.log(this.mobcount,"========== inside  mobcount > 6=================");
      if(this.mobcount == 1){
        this.mobdisp1='none';
        this.mobdisp2='block';
        this.mobdisp3='block';
        this.mobdisp4='block';
        this.mobdisp5='none';
        this.mobdisp6='none';
    
        this.mobobj2='-20%';
        this.mobobj3='30%';
        this.mobobj4='80%';
    
      }
     
    }
    else if(this.mobcount == 1){
      this.mobdisp1='none';
      this.mobdisp5='none';
      this.mobdisp6='none';
      this.mobdisp4='block';
  
      this.mobobj2='-20%';
      this.mobobj3='30%';
      this.mobobj4='80%';
  
    }
    else if(this.mobcount == 2){
      console.log("----------count 2 clicks---------------");
      this.mobdisp1='none';
      this.mobdisp2='none';
      this.mobdisp6='none'; 
  
      this.mobdisp3='block';
      this.mobdisp4='block';
      this.mobdisp5='block';
  
      this.mobobj3='-20%';
      this.mobobj4='30%';
      this.mobobj5='80%';
    }
    else if (this.mobcount == 3){
  
      this.mobdisp1='none';
      this.mobdisp2='none';
      this.mobdisp3='none';
  
      this.mobdisp4='block';
      this.mobdisp5='block';
      this.mobdisp6='block';
      
      this.mobobj4='-20%';
      this.mobobj5='30%';
      this.mobobj6='80%';
    }
  
    else if(this.mobcount == 4){
  
      this.mobdisp1='block';
      this.mobdisp2='none';
      this.mobdisp3='none';
      this.mobdisp4='none';
      this.mobdisp5='block';
      this.mobdisp6='block';
  
      this.mobobj5='-20%';
      this.mobobj6='30%';
      this.mobobj1='80%';
  
    }
    else if(this.mobcount == 5){
      this.mobdisp1='block';
      this.mobdisp2='block';
      this.mobdisp3='none';
      this.mobdisp4='none';
      this.mobdisp5='none';
      this.mobdisp6='block';

      this.mobobj6='-20%';
      this.mobobj1='30%';
      this.mobobj2='80%';

    }
    else if(this.mobcount == 6){
      this.mobdisp1='block';
      this.mobdisp2='block';
      this.mobdisp3='block';
      this.mobdisp4='none';
      this.mobdisp5='none';
      this.mobdisp6='none';

      this.mobobj1='-20%';
      this.mobobj2='30%';
      this.mobobj3='80%';
    }
  
  
}



 // mobile display div of testimonials
 mobt1:string='block';
 mobt2:string='none';
 mobt3:string='none';


 mobtestCount:number = 0;

 mobforwardBtn:boolean = true;
 mobbackwardBtn:boolean = false;

 mobtestBackwardClick(){

this.mobtestCount =this.mobtestCount-1;
console.log(this.mobtestCount,"backward button clicked");
  
if(this.mobtestCount > 2){
     this.mobtestCount = this.mobtestCount % 2;
   }

   else if(this.mobtestCount < 2){
            if(this.mobtestCount == 0){
     //  this.msg.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
 
     this.mobt1='block';
     this.mobt2='none';
     this.mobt3='none';

   this.mobtestCount =0;
   this.mobbackwardBtn=false;
   this.mobforwardBtn=true;

   document.getElementById('mobidt1').className ='mobani-active';
    }
  else if(this.mobtestCount == 1 ){
             
              this.mobt1='none';
              this.mobt2='block';
              this.mobt3='none';

              this.mobforwardBtn=true;

   document.getElementById('mobidt2').className ='mobani-active';
            }
   }
 }



 mobtestForwardClick(){
  //  this.msg.add({severity:'success', summary: 'Success Message', detail:'Order submitted'});
  console.log("forward button clicked");
   this.mobtestCount=this.mobtestCount+1;

   console.log(this.mobtestCount,"================= forwardCount ===============");
   if(this.mobtestCount == 1){
    
     this.mobt1='none';
     this.mobt2='block';
     this.mobt3='none';

     this.mobbackwardBtn=true;

     document.getElementById('mobidt2').className ='mobani';

   }
   else if(this.mobtestCount == 2){

     this.mobt1='none';
     this.mobt2='none';
     this.mobt3='block';

     this.mobforwardBtn=false;
     this.mobbackwardBtn=true;

     document.getElementById('mobidt3').className ='mobani';

   }
   
 }

}
