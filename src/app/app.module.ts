import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

//angular material imports
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';


// prime ng imports
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {ButtonModule} from 'primeng/button';
import {MenuItem} from 'primeng/api';                 //api
import {CardModule} from 'primeng/card';
import {CarouselModule} from 'primeng/carousel';
import {MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ToastModule,
    CarouselModule,
    MatCardModule,
    CardModule,
    ButtonModule,
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatButtonModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
